<?php
/**
 * Created by PhpStorm.
 * User: quangthinh
 * Date: 11/10/2016
 * Time: 11:08 AM
 */

namespace quangthinh\yii\sync\controllers;

use quangthinh\yii\sync\Module;
use Yii;
use yii\db\Query;
use yii\rest\Controller;

class TrackController extends Controller
{
    /**
     * load change of table
     */
    public function actionIndex($table_name, $___v = 0)
    {
        $module = Module::getInstance();
        $changed = [];
        $deleted = [];

        // null => all
        if (! $___v) {
            $___v = '1990-01-01 00:00:00';
        }

        if (in_array($table_name, $module->monitorTables)) {
            $changed = $this->loadChanged($table_name, $___v);
            $deleted = $this->loadDeleted($table_name, $___v);
        }

        return [
            'changed' => $changed,
            'deleted' => $deleted,
        ];
    }

    public function actionMigrate()
    {
        $data = Yii::$app->request->post('data');
        if ($data) {
            $data = Module::getInstance()->decrypt($data);

            $response = [];
            foreach ($data as $options) {
                $table_name = $options['table_name'];
                $___v = $options['___v'];

                $itemMigrate = $this->actionIndex($table_name, $___v);
                $response[$table_name] = $itemMigrate;
            }

            return $response;
        }

        return '';
    }

    private function loadChanged($table_name, $___v)
    {
        $module = Module::getInstance();
        $db = $module->db;

        return (new Query())
            ->select('*')
            ->from($table_name)
            ->where([
                '>', '___v', $___v,
            ])
            ->orderBy(['___v' => SORT_ASC])
            ->all($db);
    }

    private function loadDeleted($table_name, $___v)
    {
        $module = Module::getInstance();
        $db = $module->db;
        $deleted_table = $module->deletedTable;

        return (new Query())
            ->select(['___id', '___v'])
            ->from($deleted_table)
            ->where([
                'table_name' => $table_name,
            ])
            ->orderBy([
                '___v' => SORT_ASC,
            ])->all($db);
    }
}