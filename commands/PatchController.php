<?php
/**
 * Created by PhpStorm.
 * User: quangthinh
 * Date: 11/10/2016
 * Time: 11:05 AM
 */

namespace quangthinh\yii\sync\commands;


use quangthinh\yii\sync\Module;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\db\Schema;
use yii\httpclient\Client;

class PatchController extends Controller
{
    public function actionBuild($t = false)
    {
        $module = Module::getInstance();
        $db = $module->db;
        $track_table = $module->trackTable;

        // each create item
        foreach ($module->monitorTables as $table_name) {
            if (!(new Query())
                ->select('*')
                ->from($track_table)
                ->where(['table_name' => $table_name])
                ->exists($db)
            ) {
                $db->createCommand()
                    ->insert($track_table, [
                        'table_name' => $table_name,
                    ])->execute();
            }

            // build alter column
            $schema = $db->schema->getTableSchema($table_name);
            if ($schema) {
                if (empty($schema->columns['___id'])) {
                    $db->createCommand()
                        ->addColumn($table_name, '___id', Schema::TYPE_STRING)
                        ->execute();
                }

                if (empty($schema->columns['___v'])) {
                    $db->createCommand()
                        ->addColumn($table_name, '___v', Schema::TYPE_DATETIME)
                        ->execute();
                }
            }

            // build trigger
            if ($t) {
                $this->buildTrigger($table_name);
            }
        }
    }

    private function buildTrigger($table_name)
    {
        $module = Module::getInstance();
        $db = $module->db;
        $deleted_table = $module->deletedTable;

        $trigger_key = md5($table_name);

        // drop
        $sql = <<<SQL
    drop trigger if exists sync__{$trigger_key}__inserted;
    drop trigger if exists sync__{$trigger_key}__updated;
    drop trigger if exists sync__{$trigger_key}__deleted;

    create trigger sync__{$trigger_key}__inserted
    before insert on {$table_name}
    for each row
    begin
        if new.___id is null then
            set new.___id = (select uuid());
            set new.___v = UTC_TIMESTAMP();
        end if;
    end;

    create trigger sync__{$trigger_key}__updated
    before update on {$table_name}
    for each row
    begin
        if new.___v is null then
          set new.___v = UTC_TIMESTAMP();
        end if;
    end;

    create trigger sync__{$trigger_key}__deleted
    after delete on {$table_name}
    for each row
    begin
      insert into {$deleted_table} (table_name, ___id, ___v) values (:table_name_alias, old.___id, old.___v);
    end;
SQL;

        $db->createCommand($sql, [
            'table_name_alias' => $table_name,
        ])->execute();
    }


    public function actionMonitor()
    {
        $module = Module::getInstance();
        $db = $module->db;
        $track_table = $module->trackTable;
        $deleted_table = $module->deletedTable;

        $client = $module->sender;
        if (! $client) {
            echo 'Invalid Client';
        } else {
            while (true) {
                $monitors = (new Query())
                    ->select('*')
                    ->from($track_table)
                    ->all();

                $response = $client->post($module->endPointUrl, [
                    'data' => $module->encrypt($monitors),
                ])->send();

                if ($response->isOk) {
                    // load change
                    $content = $response->data;
                    foreach ($content as $table_name => $migrations) {
                        $this->applyChange($table_name, $migrations);
                    }
                }
            }
        }
    }

    private function applyChange($table_name, $migrations)
    {
        $last_time = 0;
        // change first
        $module = Module::getInstance();
        $track_table = $module->trackTable;
        $db = $module->db;

        $transaction = $db->beginTransaction();

        try {

            foreach ($migrations['changed'] as $item) {
                $last_time = max($this->applyChangeItem($table_name, $item), $last_time);
            }

            foreach ($migrations['deleted'] as $item) {
                $last_time = max($this->applyDeleteItem($table_name, $item), $last_time);
            }

            // update track key
            if ((new Query())
                ->select('*')
                ->from($track_table)
                ->where(['table_name' => $table_name])
                ->exists($db)
            ) {
                $db->createCommand()
                    ->update($track_table, [
                        '___v' => $last_time,
                    ], [
                        'table_name' => $table_name,
                    ])->execute();
            } else {
                $db->createCommand()
                    ->insert($track_table, [
                        'table_name' => $table_name,
                        '___v' => $last_time,
                    ])->execute();
            }


            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollBack();
        }
    }

    private function applyChangeItem($table_name, $item)
    {
        $module = Module::getInstance();
        $db = $module->db;

        // update or insert ?
        if ((new Query())
            ->select('*')
            ->from($table_name)
            ->where([
                '___id' => $item['___id'],
            ])->exists($db)
        ) {
            $db->createCommand()
                ->update($table_name, $item, [
                    '___id' => $item['___id'],
                ])->execute();
        } else {
            $db->createCommand()
                ->insert($table_name, $item)
                ->execute();
        }

        return $item['___v'];
    }

    private function applyDeleteItem($table_name, $item)
    {
        $module = Module::getInstance();
        $db = $module->db;

        // only delete
        $db->createCommand()
            ->delete($table_name, $item)
            ->execute();

        return $item['___v'];
    }
}