<?php

use yii\db\Migration;

/**
 * Handles the creation of table `track`.
 */
class m161111_023908_create_track_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%__track_version}}', [
            'table_name' => $this->string(200),
            '___v' => $this->dateTime(),
        ]);

        $this->createTable('{{%__track_delete}}', [
            'table_name' => $this->string(200),
            '___id' => $this->string(36),
            '___v' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdocy
     *
     */
    public function down()
    {
        $this->dropTable('{{%__track_delete}}');
        $this->dropTable('{{%__track_version}}');
    }
}
